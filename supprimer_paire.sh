#!/bin/bash

if [ $# -eq 2 ]
  then
	fichier=disque/file.txt
	if [ -f "$fichier" ]; 
		then
			deleteline="("$1","$2")"
			if grep -q "$deleteline" "$fichier";
			  then
			  	# effacer une plage de texte comprise entre les arguments entrés et la sortie redirigé vers un tmp
				sed "/$deleteline/d" $fichier > disque/tmp.txt 
				rm $fichier 
				mv disque/tmp.txt $fichier 

				echo "Pairs ("$1","$2") has been deleted"
			   else
			   	echo "can't find the pairs "
			  fi

		else
			echo "Error"
	fi
   else
		echo "error you need 2 arguments "
fi

