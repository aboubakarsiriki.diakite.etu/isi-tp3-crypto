#!/bin/bash

rm -rf disque ramDisk usb1 usb2
mkdir disque ramDisk usb1 usb2
touch disque/file.txt


##crer des fichiers txt avec une sortie codée 64
dd if=/dev/random  of=ramDisk/key1.txt bs=64 count=16  && dd if=/dev/random  of=ramDisk/key2.txt bs=64 count=16 

echo "cle 1"
openssl enc -pbkdf2 -aes-256-cbc  -in ramDisk/key1.txt -out usb1/key1.enc -pass pass:test1

echo "cle 2"
openssl enc -pbkdf2 -aes-256-cbc  -in ramDisk/key2.txt -out usb2/key2.enc -pass pass:test2

#The first line of pathname is the password. If the same pathname
# argument is supplied to -passin and -passout arguments then the
#first line will be used for the input password and the next line for the output password.
openssl enc -pbkdf2 -aes-256-cbc  -in disque/file.txt -out disque/file1Crypt.enc -kfile ramDisk/key1.txt && openssl enc -pbkdf2 -aes-256-cbc  -in disque/file1Crypt.enc  -out disque/fileCrypt.enc -kfile ramDisk/key2.txt


rm ramDisk/key1.txt
rm ramDisk/key2.txt
rmdir ramDisk
rm disque/file1Crypt.enc



