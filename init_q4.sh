#!/bin/bash

rm -rf disque ramDisk rep1 rep2 usb1 usb2
mkdir disque ramDisk usb1 usb2 rep1 rep2



##crer des fichiers txt avec une sortie codée 64
dd if=/dev/random  of=ramDisk/key1.txt bs=64 count=16  && dd if=/dev/random  of=ramDisk/key2.txt bs=64 count=16 

echo "cle 1"
## on encode u format aes le contenu de key1.txt en key1.enc (.enc a cause du enc de openssl  dans le dossier usb1) avec comme mot de passe test1
openssl enc -pbkdf2 -aes-256-cbc  -in ramDisk/key1.txt -out usb1/key1.enc -pass pass:test1

echo "cle 2"
openssl enc -pbkdf2 -aes-256-cbc  -in ramDisk/key2.txt -out usb2/key2.enc -pass pass:test2


#on deplace le contenu des fichier text ayant servi au chiffrement en leur changeant de nom qui correspondent aux fichier des representant
mv ramDisk/key1.txt  ramDisk/repkey1.txt && mv ramDisk/key2.txt  ramDisk/repkey2.txt
## ajouter les mots rep au début du fichier pour identifier que c'est un representnt
echo "rep 1">> ramDisk/repkey1.txt && echo "rep 1">> ramDisk/repkey1.txt

echo "cle rep 1"
#et oin encode aussi
openssl enc -pbkdf2 -aes-256-cbc  -in ramDisk/repkey1.txt -out rep1/repkey1.enc -pass pass:test1

echo "cle  rep 2"
openssl enc -pbkdf2 -aes-256-cbc  -in ramDisk/repkey2.txt -out rep2/repkey2.enc -pass pass:test2

openssl enc -pbkdf2 -aes-256-cbc  -in disque/file.txt -out disque/file1Crypt.enc -kfile ramDisk/repkey1.txt && openssl enc -pbkdf2 -aes-256-cbc  -in disque/file1Crypt.enc  -out disque/fileCrypt.enc -kfile ramDisk/repkey2.txt



